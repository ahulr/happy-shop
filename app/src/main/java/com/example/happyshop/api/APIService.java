package com.example.happyshop.api;

import com.example.happyshop.model.CategoryResponse;
import com.example.happyshop.model.Product;
import com.example.happyshop.model.ProductResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ahulr on 21-08-2017.
 */

public interface APIService {

    //Get Products
    @GET("api/v1/products")
    Observable<CategoryResponse> getProducts(@Query("category") String category, @Query("page") int page);

    //Get Product
    @GET("api/v1/products/{id}")
    Observable<ProductResponse> getSingleProduct(@Path("id") Integer id);

}
