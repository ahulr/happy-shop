package com.example.happyshop.data;

import android.app.Activity;
import android.app.Application;

import com.example.happyshop.model.Cart;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ahulr on 22-08-2017.
 */

public class RealmHelper {

    private static RealmHelper instance;
    private final Realm realm;

    public RealmHelper(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmHelper with(Activity activity) {

        if (instance == null) {
            instance = new RealmHelper(activity.getApplication());
        }
        return instance;
    }

    public static RealmHelper with(Application application) {

        if (instance == null) {
            instance = new RealmHelper(application);
        }
        return instance;
    }

    public static RealmHelper getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    public void refresh() {
        realm.refresh();
    }

    public RealmResults<Cart> getAllProducts() {
        return realm.where(Cart.class).findAll();
    }

    public Cart getProduct(Integer id) {
        return realm.where(Cart.class).equalTo("id", id).findFirst();
    }

    public int getCartSize() {

        int count = 0;
        RealmResults<Cart> cartItems = realm.where(Cart.class).findAll();
        if (cartItems != null) {
            for (int i = 0; i < cartItems.size(); i++) {
                count = count + cartItems.get(i).getQuantity();
            }
        }

        return count;
    }

    public void addProduct(Cart product) {

        Cart check = realm.where(Cart.class).equalTo("id", product.getId()).findFirst();
        if (check == null) {
            realm.beginTransaction();
            realm.copyToRealm(product);
            realm.commitTransaction();
        } else {
            realm.beginTransaction();
            int qty = check.getQuantity();
            check.setQuantity(qty + 1);
            realm.commitTransaction();
        }
    }

    public void removeProduct(Cart product) {
        RealmResults<Cart> result = realm.where(Cart.class).equalTo("id", product.getId()).findAll();
        realm.beginTransaction();
        result.deleteAllFromRealm();
        realm.commitTransaction();
    }


    public void clearAll() {
        realm.deleteAll();
    }

    /*public boolean hasProducts() {
        return !realm.allObjects(Product.class).isEmpty();
    }*/
}