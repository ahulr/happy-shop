package com.example.happyshop.dagger.component;

import com.example.happyshop.api.APIService;
import com.example.happyshop.app.HappyShop;
import com.example.happyshop.dagger.module.ServiceModule;
import com.example.happyshop.dagger.scope.ApplicationScope;
import com.example.happyshop.model.Category;
import com.example.happyshop.model.Product;
import com.example.happyshop.view.category.CategoryActivity;
import com.example.happyshop.view.home.HomeActivity;
import com.example.happyshop.view.product.ProductActivity;

import dagger.Component;

/**
 * Created by ahulr on 21-08-2017.
 */

@ApplicationScope
@Component(modules = {ServiceModule.class})
public interface AppComponent {

    APIService apiService();

    void inject(CategoryActivity categoryActivity);

    void inject(ProductActivity productActivity);

}
