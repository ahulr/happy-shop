package com.example.happyshop.dagger.module;

import com.example.happyshop.api.APIService;
import com.example.happyshop.dagger.scope.ApplicationScope;
import com.example.happyshop.utility.Constants;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ahulr on 21-08-2017.
 */

@Module(includes = NetworkModule.class)
public class ServiceModule {

    @ApplicationScope
    @Provides
    public Retrofit retrofit(OkHttpClient httpClient) {
        return provideRetrofit(httpClient);
    }

    public static Retrofit provideRetrofit(OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(Constants.ENDPOINT)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @ApplicationScope
    @Provides
    public APIService apiService(Retrofit retrofit) {
        return retrofit.create(APIService.class);
    }

}
