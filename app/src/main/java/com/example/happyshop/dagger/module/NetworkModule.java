package com.example.happyshop.dagger.module;

import android.content.Context;

import com.example.happyshop.dagger.scope.ApplicationScope;
import com.example.happyshop.utility.Constants;
import com.example.happyshop.utility.NetworkUtils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by ahulr on 21-08-2017.
 */

@Module(includes = ContextModule.class)
public class NetworkModule {

    @ApplicationScope
    @Provides
    public File file(Context context) {
        return new File(context.getCacheDir(), "cache");
    }

    @ApplicationScope
    @Provides
    public Cache cache(File file) {
        return new Cache(file, 10 * 1024 * 1024);
    }

    @ApplicationScope
    @Provides
    public Interceptor interceptor(final Context context) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder builder = original.newBuilder()
                        .addHeader("Content-Type", "application/json");
                Request request = builder.build();

                return chain.proceed(request);
            }
        };
    }

    @ApplicationScope
    @Provides
    public HttpLoggingInterceptor httpLoggingInterceptor() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @ApplicationScope
    @Provides
    public OkHttpClient okHttpClient(Interceptor interceptor, HttpLoggingInterceptor httpLoggingInterceptor, Cache cache) {

        return new OkHttpClient.Builder()
                .readTimeout(Constants.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(Constants.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .addNetworkInterceptor(interceptor)
                .addInterceptor(httpLoggingInterceptor)
                .cache(cache)
                .build();
    }


}
