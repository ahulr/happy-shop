package com.example.happyshop.dagger.module;

import android.app.Application;
import android.content.Context;

import com.example.happyshop.app.HappyShop;
import com.example.happyshop.dagger.scope.ApplicationScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ahulr on 21-08-2017.
 */

@Module
public class ContextModule {

    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @ApplicationScope
    @Provides
    public Context context() {
        return context;
    }

}
