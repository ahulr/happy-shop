package com.example.happyshop.app;

import android.app.Application;
import android.content.Context;

import com.example.happyshop.R;
import com.example.happyshop.dagger.component.AppComponent;
import com.example.happyshop.dagger.component.DaggerAppComponent;
import com.example.happyshop.dagger.module.ContextModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by ahulr on 21-08-2017.
 */

public class HappyShop extends Application {

    private static HappyShop sInstance;
    private static AppComponent appComponent;

    public static HappyShop get(Context context) {
        return (HappyShop) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
        initCalligraphy();
        initRealm();
    }

    private void initAppComponent(){
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();
        }
    }

    private void initCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Futura-Bold.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(AppComponent appComponent) {
        this.appComponent = appComponent;
    }

    public static HappyShop getInstance() {
        if (sInstance == null) {
            sInstance = new HappyShop();
        }
        return sInstance;
    }
}
