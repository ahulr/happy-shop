package com.example.happyshop.viewmodel;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;

import com.example.happyshop.R;
import com.example.happyshop.adapter.CategoryAdapter;
import com.example.happyshop.model.Category;
import com.squareup.picasso.Picasso;

/**
 * Created by ahulr on 21-08-2017.
 */

public class CategoryViewModel {

    private Category category;

    public CategoryViewModel(Category category) {
        this.category = category;
    }

    public String getTitle() {
        return TextUtils.isEmpty(category.getTitle()) ? "" : category.getTitle();
    }

    public int getImageRes() {
        return category.getImgRes();
    }

    @BindingAdapter({"bind:imageRes"})
    public static void imageRes(ImageView view, int imageRes) {
        if (imageRes != 0) {
            Picasso.with(view.getContext())
                    .load(imageRes)
                    .fit()
                    .centerCrop()
                    .into(view);
        }
    }
}
