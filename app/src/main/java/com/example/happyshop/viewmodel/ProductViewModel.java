package com.example.happyshop.viewmodel;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.example.happyshop.R;
import com.example.happyshop.adapter.ProductAdapter;
import com.example.happyshop.model.Product;
import com.squareup.picasso.Picasso;

/**
 * Created by ahulr on 21-08-2017.
 */

public class ProductViewModel {

    private Product product;

    public ProductViewModel(Product product) {
        this.product = product;
    }

    public String getName() {
        return TextUtils.isEmpty(product.getName()) ? "" : product.getName();
    }

    public String getPrice() {
        return "S$" + String.valueOf(product.getPrice());
    }

    public boolean getOffer() {
        return product.getUnderSale();
    }

    @BindingAdapter("android:visibility")
    public static void getOffer(View view, Boolean offer) {
        view.setVisibility(offer ? View.VISIBLE : View.GONE);
    }

    public String getImageUrl() {
        return product.getImgUrl();
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void imageUrl(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(view.getContext())
                    .load(imageUrl)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.error)
                    .centerInside()
                    .fit()
                    .into(view);
        }
    }
}
