package com.example.happyshop.viewmodel;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;

import com.example.happyshop.model.Category;
import com.squareup.picasso.Picasso;

/**
 * Created by ahulr on 21-08-2017.
 */

public class FilterViewModel {

    private Category category;

    public FilterViewModel(Category category) {
        this.category = category;
    }

    public String getTitle() {
        return TextUtils.isEmpty(category.getTitle()) ? "" : category.getTitle();
    }
}
