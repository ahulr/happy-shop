package com.example.happyshop.viewmodel;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.example.happyshop.R;
import com.example.happyshop.adapter.CartAdapter;
import com.example.happyshop.model.Cart;
import com.squareup.picasso.Picasso;

/**
 * Created by ahulr on 21-08-2017.
 */

public class CartViewModel {

    private Cart cart;

    public CartViewModel(Cart cart) {
        this.cart = cart;
    }

    public String getName() {
        return TextUtils.isEmpty(cart.getProduct().getName()) ? "" : cart.getProduct().getName();
    }

    public String getPrice() {
        return "S$" + String.valueOf(cart.getProduct().getPrice());
    }

    public String getQty() {
        return "Qty: " + String.valueOf(cart.getQuantity());
    }

    public String getImageUrl() {
        return cart.getProduct().getImgUrl();
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void imageUrl(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(view.getContext())
                    .load(imageUrl)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.error)
                    .centerInside()
                    .fit()
                    .into(view);
        }
    }

}
