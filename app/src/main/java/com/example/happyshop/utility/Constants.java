package com.example.happyshop.utility;

/**
 * Created by ahulr on 21-08-2017.
 */

public class Constants {

    public static String ENDPOINT = "http://sephora-mobile-takehome-apple.herokuapp.com/";
    public static long DEFAULT_TIMEOUT = 60;

}
