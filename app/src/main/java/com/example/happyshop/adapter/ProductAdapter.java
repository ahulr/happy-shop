package com.example.happyshop.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;

import com.example.happyshop.R;
import com.example.happyshop.databinding.ItemProductBinding;
import com.example.happyshop.model.Product;
import com.example.happyshop.utility.DeviceUtils;
import com.example.happyshop.viewmodel.ProductViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahulr on 21-08-2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.BindingHolder> {

    private List<Product> productList = new ArrayList<>();
    private Context mContext;
    private int lastAnimatedPosition = -1;
    private final int ANIMATED_ITEMS_COUNT = 7;
    private ProductClickListener productClickListener;

    public ProductAdapter(Context context) {
        this.mContext = context;
    }

    public interface ProductClickListener {
        void onProductClicked(Product product);
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemProductBinding productBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_product,
                parent,
                false);
        return new BindingHolder(productBinding);
    }

    //Used Data binding to set values directly to the view item
    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        runEnterAnimation(holder.itemView, position);
        ItemProductBinding productBinding = holder.binding;
        ProductViewModel productViewModel = new ProductViewModel(productList.get(position));
        productBinding.setViewModel(productViewModel);

    }

    private void runEnterAnimation(View view, int position) {
        if (position >= ANIMATED_ITEMS_COUNT - 1) {
            return;
        }

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(DeviceUtils.getScreenHeight(mContext));
            view.animate()
                    .translationY(0)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(1000)
                    .start();
        }
    }

    public void setProducts(List<Product> list) {
        productList.clear();
        notifyDataSetChanged();
        addProducts(list);
    }

    public void addProducts(List<Product> list) {
        for (int i = 0; i < list.size(); i++) {
            addItem(list.get(i));
        }
    }

    public void addItem(Product product) {
        if (!productList.contains(product)) {
            productList.add(product);
            notifyItemInserted(productList.size() - 1);
        } else {
            productList.set(productList.indexOf(product), product);
            notifyItemChanged(productList.indexOf(product));
        }
    }


    public void setItemClickListener(ProductClickListener productClickListener) {
        this.productClickListener = productClickListener;
    }


    @Override
    public int getItemCount() {
        return productList.size();
    }


    public class BindingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemProductBinding binding;

        public BindingHolder(ItemProductBinding binding) {
            super(binding.itemProduct);
            binding.itemProduct.setOnClickListener(this);
            this.binding = binding;
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            if (productClickListener != null) {
                productClickListener.onProductClicked(productList.get(pos));
            }
        }
    }
}
