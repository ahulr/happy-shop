package com.example.happyshop.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.happyshop.R;
import com.example.happyshop.databinding.ItemCartBinding;
import com.example.happyshop.model.Cart;
import com.example.happyshop.model.Category;
import com.example.happyshop.viewmodel.CartViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahulr on 21-08-2017.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.BindingHolder> {

    private List<Cart> cartList = new ArrayList<>();
    private Context mContext;
    private CartClickListener cartClickListener;

    public CartAdapter(Context context) {
        this.mContext = context;
    }

    public interface CartClickListener {
        void onCartItemRemoved(Cart cart);

    }

    @Override
    public CartAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCartBinding cartBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_cart,
                parent,
                false);
        return new CartAdapter.BindingHolder(cartBinding);
    }

    //Used Data binding to set values directly to the view item
    @Override
    public void onBindViewHolder(CartAdapter.BindingHolder holder, final int position) {
        ItemCartBinding cartBinding = holder.binding;
        CartViewModel cartViewModel = new CartViewModel(cartList.get(position));
        cartBinding.setViewModel(cartViewModel);

        holder.binding.itemCartRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cartClickListener != null) {
                    cartClickListener.onCartItemRemoved(cartList.get(position));
                }

            }
        });

    }

    public void setCartItems(List<Cart> list) {
        cartList.clear();
        notifyDataSetChanged();
        addCartItems(list);
    }

    public void addCartItems(List<Cart> list) {
        for (int i = 0; i < list.size(); i++) {
            addItem(list.get(i));
        }
    }

    public void addItem(Cart cart) {
        if (!cartList.contains(cart)) {
            cartList.add(cart);
            notifyItemInserted(cartList.size() - 1);
        } else {
            cartList.set(cartList.indexOf(cart), cart);
            notifyItemChanged(cartList.indexOf(cart));
        }
    }


    public void setItemClickListener(CartClickListener cartClickListener) {
        this.cartClickListener = cartClickListener;
    }


    @Override
    public int getItemCount() {
        return cartList.size();
    }


    public class BindingHolder extends RecyclerView.ViewHolder {
        private ItemCartBinding binding;

        public BindingHolder(ItemCartBinding binding) {
            super(binding.itemCart);
            this.binding = binding;
        }
    }
}
