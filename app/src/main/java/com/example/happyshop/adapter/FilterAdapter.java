package com.example.happyshop.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.happyshop.R;
import com.example.happyshop.databinding.ItemFilterBinding;
import com.example.happyshop.model.Category;
import com.example.happyshop.viewmodel.FilterViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahulr on 21-08-2017.
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.BindingHolder> {

    private List<Category> categoryList = new ArrayList<>();
    private Context mContext;
    private CategoryClickListener categoryClickListener;

    public FilterAdapter(Context context) {
        this.mContext = context;
    }

    public interface CategoryClickListener {
        void onCategoryClicked(Category category);
    }

    @Override
    public FilterAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemFilterBinding filterBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_filter,
                parent,
                false);
        return new FilterAdapter.BindingHolder(filterBinding);
    }

    //Used Data binding to set values directly to the view item
    @Override
    public void onBindViewHolder(FilterAdapter.BindingHolder holder, int position) {
        ItemFilterBinding filterBinding = holder.binding;
        FilterViewModel categoryViewModel = new FilterViewModel(categoryList.get(position));
        filterBinding.setViewModel(categoryViewModel);
    }

    public void setCategories(List<Category> list) {
        categoryList.clear();
        notifyDataSetChanged();
        addCategories(list);
    }

    public void addCategories(List<Category> list) {
        for (int i = 0; i < list.size(); i++) {
            addItem(list.get(i));
        }
    }

    public void addItem(Category category) {
        if (!categoryList.contains(category)) {
            categoryList.add(category);
            notifyItemInserted(categoryList.size() - 1);
        } else {
            categoryList.set(categoryList.indexOf(category), category);
            notifyItemChanged(categoryList.indexOf(category));
        }
    }


    public void setItemClickListener(CategoryClickListener categoryClickListener) {
        this.categoryClickListener = categoryClickListener;
    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }


    public class BindingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemFilterBinding binding;

        public BindingHolder(ItemFilterBinding binding) {
            super(binding.itemFilter);
            binding.itemFilter.setOnClickListener(this);
            this.binding = binding;
        }

        @Override
        public void onClick(View view) {
            if (categoryClickListener != null) {
                int pos = getAdapterPosition();
                categoryClickListener.onCategoryClicked(categoryList.get(pos));
            }
        }
    }
}