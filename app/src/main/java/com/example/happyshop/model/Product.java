package com.example.happyshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by ahulr on 21-08-2017.
 */

public class Product extends RealmObject implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("under_sale")
    @Expose
    private Boolean underSale;

    public Product() {
    }

    public Product(Integer id, String name, String category, Double price, String imgUrl, String description, Boolean underSale) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.imgUrl = imgUrl;
        this.description = description;
        this.underSale = underSale;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getUnderSale() {
        return underSale;
    }

    public void setUnderSale(Boolean underSale) {
        this.underSale = underSale;
    }
}
