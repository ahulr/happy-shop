package com.example.happyshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahulr on 22-08-2017.
 */

public class ProductResponse {
    @SerializedName("product")
    @Expose
    private Product product;

    public ProductResponse() {
    }

    public ProductResponse(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
