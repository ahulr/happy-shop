package com.example.happyshop.model;

/**
 * Created by ahulr on 21-08-2017.
 */

public class Category {

    public String title;
    public int imgRes;

    public Category() {
    }

    public Category(String title, int imgRes) {
        this.title = title;
        this.imgRes = imgRes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }
}
