package com.example.happyshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ahulr on 22-08-2017.
 */

public class CategoryResponse {

    @SerializedName("products")
    @Expose
    private List<Product> products = null;

    public CategoryResponse() {
    }

    public CategoryResponse(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}
