package com.example.happyshop.view.product;

import com.example.happyshop.api.APIService;
import com.example.happyshop.app.HappyShop;
import com.example.happyshop.model.Cart;
import com.example.happyshop.model.Product;
import com.example.happyshop.model.ProductResponse;
import com.example.happyshop.data.RealmHelper;
import com.example.happyshop.view.base.BaseView;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ahulr on 21-08-2017.
 */

public class ProductPresenter implements ProductContract.Presenter {

    private APIService apiService;
    private ProductContract.View view;

    public ProductPresenter(APIService apiService) {
        this.apiService = apiService;
    }

    @Override
    public void loadProduct(Integer id) {
        view.showLoading();
        apiService.getSingleProduct(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull ProductResponse productResponse) {
                        view.hideLoading();
                        view.setProduct(productResponse.getProduct());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.hideLoading();
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void addToCart(Product product) {
        if (product != null) {

            try {
                HappyShop happyShop = HappyShop.getInstance();
                RealmHelper realmHelper = RealmHelper.with(happyShop).getInstance();

                Cart cart = new Cart();
                cart.setId(product.getId());
                cart.setProduct(product);
                cart.setQuantity(1);

                realmHelper.addProduct(cart);

                view.productAddedToCart();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void setView(BaseView view) {
        this.view = (ProductContract.View) view;
    }
}
