package com.example.happyshop.view.product;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.happyshop.R;
import com.example.happyshop.api.APIService;
import com.example.happyshop.app.HappyShop;
import com.example.happyshop.model.Product;
import com.example.happyshop.utility.Extra;
import com.example.happyshop.view.base.BaseActivity;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductActivity extends BaseActivity implements ProductContract.View {

    @BindView(R.id.activity_product_toolbar)
    Toolbar activityProductToolbar;
    @BindView(R.id.activity_product_image)
    ImageView activityProductImage;
    @BindView(R.id.activity_product_name)
    TextView activityProductName;
    @BindView(R.id.activity_product_price)
    TextView activityProductPrice;
    @BindView(R.id.activity_product_offer)
    TextView activityProductOffer;
    @BindView(R.id.activity_product_add_cart)
    Button activityProductAddCart;
    @BindView(R.id.activity_product_description)
    TextView activityProductDescription;
    @BindView(R.id.activity_product_progress)
    ProgressBar activityProductProgress;
    @BindView(R.id.activity_product)
    RelativeLayout activityProduct;
    private Product product;
    private ProductPresenter productPresenter;

    @Inject
    public APIService apiService;


    public static Intent getStartIntent(Context context, Product product) {
        Intent intent = new Intent(context, ProductActivity.class);
        intent.putExtra(Extra.PRODUCT, product);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);

        setToolbar();

        if (getIntent().hasExtra(Extra.PRODUCT)) {
            product = (Product) getIntent().getSerializableExtra(Extra.PRODUCT);
            setProduct(product);
            productPresenter.loadProduct(product.getId());
        }
    }

    public void setToolbar() {
        setSupportActionBar(activityProductToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
    }

    @Override
    public void setView() {
        HappyShop.getInstance().getAppComponent().inject(this);
        productPresenter = new ProductPresenter(apiService);
        productPresenter.setView(this);
    }

    @Override
    public void showLoading() {
        activityProductProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        activityProductProgress.setVisibility(View.GONE);
    }

    @Override
    public void showError(String s) {
        showSnackbar(activityProduct, s);
    }

    @Override
    public void setProduct(Product product) {

        this.product = product;

        if (!TextUtils.isEmpty(product.getName())) {
            setToolbarTitle(product.getName());
        }

        if (!TextUtils.isEmpty(product.getImgUrl())) {
            Picasso.with(this).load(product.getImgUrl())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.error)
                    .into(activityProductImage);
        }

        if (product.getUnderSale() != null) {
            activityProductOffer.setVisibility(product.getUnderSale() ? View.VISIBLE : View.INVISIBLE);
        }

        activityProductPrice.setText(product.getPrice() != null ? "S$" + String.valueOf(product.getPrice()) : "");
        activityProductName.setText(TextUtils.isEmpty(product.getName()) ? "" : product.getName());
        activityProductDescription.setText(TextUtils.isEmpty(product.getDescription()) ? "" : product.getDescription());
    }

    @Override
    public void productAddedToCart() {
        updateCartCount();
        showSnackbar(activityProduct, getResources().getString(R.string.success_cart));
    }

    @OnClick(R.id.activity_product_add_cart)
    public void addToCart() {
        productPresenter.addToCart(product);
    }

    @OnClick(R.id.activity_product_image)
    public void openImageZoom() {
        ImageZoomDialog imageZoomDialog = new ImageZoomDialog(this, product.getImgUrl());
        imageZoomDialog.show();
    }

    private void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
