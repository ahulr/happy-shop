package com.example.happyshop.view.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.RelativeLayout;

import com.example.happyshop.R;
import com.example.happyshop.adapter.CategoryAdapter;
import com.example.happyshop.model.Category;
import com.example.happyshop.view.base.BaseActivity;
import com.example.happyshop.view.category.CategoryActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements HomeContract.View {

    @BindView(R.id.activity_home_recycler_view)
    RecyclerView activityHomeRecyclerView;
    @BindView(R.id.activity_home)
    RelativeLayout activityHome;
    @BindView(R.id.activity_home_toolbar)
    Toolbar activityHomeToolbar;

    private CategoryAdapter categoryAdapter;
    private LinearLayoutManager linearLayoutManager;
    private HomePresenter homePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setToolbar();
        setView();
        setAdapter();
        homePresenter.loadCategories();
    }

    public void setToolbar() {
        setSupportActionBar(activityHomeToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_menu);
        getSupportActionBar().setTitle(R.string.app_name);
    }

    @Override
    public void setView() {
        homePresenter = new HomePresenter();
        homePresenter.setView(this);
    }

    private void setAdapter() {
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        activityHomeRecyclerView.setLayoutManager(linearLayoutManager);
        categoryAdapter = new CategoryAdapter(this);
        activityHomeRecyclerView.setAdapter(categoryAdapter);
        categoryAdapter.setItemClickListener(new CategoryAdapter.CategoryClickListener() {
            @Override
            public void onCategoryClicked(Category category) {
                openCategory(category.getTitle());
            }
        });
    }

    @Override
    public void showLoading() {
        addProgressLayout(this, activityHome);
    }

    @Override
    public void hideLoading() {
        removeProgressLayout(activityHome);
    }

    @Override
    public void showError(String s) {
        showSnackbar(activityHome, s);
    }

    @Override
    public void setCategories(List<Category> categoryList) {
        categoryAdapter.setCategories(categoryList);
    }

    private void openCategory(String category) {
        Intent intent = CategoryActivity.getCategoryIntent(this, category);
        startActivity(intent);
    }
}
