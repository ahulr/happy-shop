package com.example.happyshop.view.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.happyshop.R;
import com.example.happyshop.app.HappyShop;
import com.example.happyshop.data.RealmHelper;
import com.example.happyshop.view.cart.CartActivity;
import com.example.happyshop.custom.NotificationBadge;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by ahulr on 21-08-2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
    }

    protected void showSnackbar(View view, String str) {
        final Snackbar snackbar = Snackbar.make(view, str, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackBarColor));
        TextView tv = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTypeface(Typeface.DEFAULT);
        tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        snackbar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    public void addProgressLayout(Context context, ViewGroup parent) {
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.view_progress, null);
        parent.addView(v, parent.getChildCount(), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    public void removeProgressLayout(ViewGroup parent) {
        if (parent.findViewById(R.id.view_progress) != null) {
            parent.removeView(parent.findViewById(R.id.view_progress));
        }
    }

    public void addEmptyLayout(Context context, ViewGroup parent, int imgRes, String title) {
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.view_empty, null);

        ImageView imageView = (ImageView) v.findViewById(R.id.view_empty_image);
        imageView.setImageResource(imgRes);
        TextView textView = (TextView) v.findViewById(R.id.view_empty_title);
        textView.setText(title);

        parent.addView(v, parent.getChildCount(), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    public void removeEmptyLayout(ViewGroup parent) {
        if (parent.findViewById(R.id.view_progress) != null) {
            parent.removeView(parent.findViewById(R.id.view_progress));
        }
    }

    public void addNoInternetLayout(Context context, ViewGroup parent) {
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.view_no_internet, null);
        parent.addView(v, parent.getChildCount(), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cart, menu);

        MenuItem menuItem = menu.findItem(R.id.action_cart);
        NotificationBadge.update(this, menuItem, new NotificationBadge.Builder()
                .iconDrawable(ContextCompat.getDrawable(this, R.drawable.ic_action_cart))
                .iconTintColor(Color.WHITE)
                .textBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent))
                .textColor(Color.WHITE));
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        NotificationBadge.getBadgeTextView(menuItem).setBadgeCount(getCartCount(), true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_cart) {

            openCartActivity();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openCartActivity() {
        Intent intent = CartActivity.getCartIntent(this);
        startActivity(intent);
    }

    private int getCartCount() {
        HappyShop happyShop = HappyShop.getInstance();
        RealmHelper realmHelper = RealmHelper.with(happyShop).getInstance();
        return realmHelper.getCartSize();
    }

    public void updateCartCount() {
        invalidateOptionsMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCartCount();
    }

    public abstract void setView();
}
