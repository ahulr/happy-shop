package com.example.happyshop.view.cart;

import com.example.happyshop.app.HappyShop;
import com.example.happyshop.model.Cart;
import com.example.happyshop.data.RealmHelper;
import com.example.happyshop.view.base.BaseView;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

/**
 * Created by ahulr on 21-08-2017.
 */

public class CartPresenter implements CartContract.Presenter {

    private CartContract.View view;

    @Override
    public void loadCartItems() {

        view.showLoading();
        try {
            HappyShop happyShop = HappyShop.getInstance();
            RealmHelper realmHelper = RealmHelper.with(happyShop).getInstance();
            RealmResults<Cart> cartitems = realmHelper.getAllProducts();

            double total = 0;
            List<Cart> cartList = new ArrayList<>();
            for (int i = 0; i < cartitems.size(); i++) {

                Cart cart = cartitems.get(i);
                total = total + cart.getProduct().getPrice();
                cartList.add(cart);
            }

            if (cartList.size() > 0) {
                view.setCartItems(cartList);
                view.setCartTotal(cartList.size(), total);
            } else {
                view.showEmptyView();
            }
            view.hideLoading();

        } catch (Exception e) {
            e.printStackTrace();
            view.hideLoading();
        }

    }

    @Override
    public void removeItemFromCart(Cart cart) {
        try {
            HappyShop happyShop = HappyShop.getInstance();
            RealmHelper realmHelper = RealmHelper.with(happyShop).getInstance();
            realmHelper.removeProduct(cart);
            loadCartItems();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setView(BaseView view) {
        this.view = (CartContract.View) view;
    }
}
