package com.example.happyshop.view.cart;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.happyshop.R;
import com.example.happyshop.adapter.CartAdapter;
import com.example.happyshop.model.Cart;
import com.example.happyshop.view.base.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartActivity extends BaseActivity implements CartContract.View {

    @BindView(R.id.activity_cart_toolbar)
    Toolbar activityCartToolbar;
    @BindView(R.id.activity_cart_recycler_view)
    RecyclerView activityCartRecyclerView;
    @BindView(R.id.activity_cart)
    RelativeLayout activityCart;
    @BindView(R.id.activity_cart_item_count)
    TextView activityCartItemCount;
    @BindView(R.id.activity_cart_total)
    TextView activityCartTotal;
    @BindView(R.id.activity_cart_bottom_bar)
    CardView activityCartBottomBar;
    @BindView(R.id.activity_cart_list_card)
    CardView activityCartListCard;

    private CartPresenter cartPresenter;
    private CartAdapter cartAdapter;
    private LinearLayoutManager linearLayoutManager;

    public static Intent getCartIntent(Context context) {
        Intent intent = new Intent(context, CartActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        setToolbar();
        setAdapter();
        cartPresenter.loadCartItems();
    }

    public void setToolbar() {
        setSupportActionBar(activityCartToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
        getSupportActionBar().setTitle(R.string.cart);
    }

    @Override
    public void setView() {
        cartPresenter = new CartPresenter();
        cartPresenter.setView(this);
    }


    private void setAdapter() {
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        activityCartRecyclerView.setLayoutManager(linearLayoutManager);

        cartAdapter = new CartAdapter(this);
        cartAdapter.setItemClickListener(new CartAdapter.CartClickListener() {
            @Override
            public void onCartItemRemoved(Cart cart) {
                cartPresenter.removeItemFromCart(cart);
            }
        });

        activityCartRecyclerView.setAdapter(cartAdapter);
    }

    @Override
    public void showLoading() {
        addProgressLayout(this, activityCart);
    }

    @Override
    public void hideLoading() {
        removeProgressLayout(activityCart);
    }

    @Override
    public void showError(String s) {
        showSnackbar(activityCart, s);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_cart).setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setCartItems(List<Cart> cartList) {
        cartAdapter.setCartItems(cartList);
    }

    @Override
    public void setCartTotal(int items, double total) {

        String s = items > 1 ? " items" : " item";
        activityCartItemCount.setText(String.valueOf(items) + s);
        activityCartTotal.setText("$" + String.valueOf(total));
    }

    @Override
    public void showEmptyView() {
        addEmptyLayout(this, activityCart, R.drawable.empty_bag, getResources().getString(R.string.empty_cart));
        activityCartBottomBar.setVisibility(View.GONE);
        activityCartListCard.setVisibility(View.GONE);
    }
}
