package com.example.happyshop.view.category;

import com.example.happyshop.model.Category;
import com.example.happyshop.model.Product;
import com.example.happyshop.view.base.BasePresenter;
import com.example.happyshop.view.base.BaseView;

import java.util.List;

/**
 * Created by ahulr on 21-08-2017.
 */

public class CategoryContract {


    interface Presenter extends BasePresenter {
        void loadCategoryProducts(String category, int page);

        void loadMoreCategoryProducts(String category, int page);
    }

    public interface View extends BaseView<Presenter> {
        void setCategoryProducts(List<Product> productList);

        void addCategoryProducts(List<Product> productList);

        void showEmptyView();
    }

}
