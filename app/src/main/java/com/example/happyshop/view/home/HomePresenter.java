package com.example.happyshop.view.home;

import com.example.happyshop.R;
import com.example.happyshop.model.Category;
import com.example.happyshop.view.base.BaseView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahulr on 22-08-2017.
 */

public class HomePresenter implements HomeContract.Presenter {

    private HomeContract.View view;
    public static List<Category> categoryList;

    public static String categoryTitle[] = {"Makeup", "Skin Care", "Hair", "Bath & Body", "Men", "Gifts"};
    public static int categoryImage[] = {R.drawable.makeup, R.drawable.skincare, R.drawable.hair, R.drawable.bath, R.drawable.men, R.drawable.gifts};

    @Override
    public void loadCategories() {
        view.setCategories(getCategoryList());
    }

    public static List<Category> getCategoryList() {

        if (categoryList != null) {
            return categoryList;
        } else {

            categoryList = new ArrayList<>();
            for (int i = 0; i < categoryTitle.length; i++) {
                Category category = new Category();
                category.setImgRes(categoryImage[i]);
                category.setTitle(categoryTitle[i]);
                categoryList.add(category);
            }

            return categoryList;
        }
    }

    @Override
    public void setView(BaseView view) {
        this.view = (HomeContract.View) view;
    }
}
