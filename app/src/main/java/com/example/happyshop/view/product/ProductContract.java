package com.example.happyshop.view.product;

import com.example.happyshop.model.Product;
import com.example.happyshop.view.base.BasePresenter;
import com.example.happyshop.view.base.BaseView;

/**
 * Created by ahulr on 21-08-2017.
 */

public class ProductContract {

    public interface Presenter extends BasePresenter {
        void loadProduct(Integer id);
        void addToCart(Product product);
    }

    public interface View extends BaseView<Presenter> {
        void setProduct(Product product);
        void productAddedToCart();
    }

}
