package com.example.happyshop.view.base;

/**
 * Created by ahulr on 21-08-2017.
 */

public interface BasePresenter<V extends BaseView> {

    void setView(V view);

}
