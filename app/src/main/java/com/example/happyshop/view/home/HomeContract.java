package com.example.happyshop.view.home;

import com.example.happyshop.model.Category;
import com.example.happyshop.view.base.BasePresenter;
import com.example.happyshop.view.base.BaseView;

import java.util.List;

/**
 * Created by ahulr on 21-08-2017.
 */

public class HomeContract {

    public interface Presenter extends BasePresenter {
        void loadCategories();
    }

    public interface View extends BaseView<Presenter> {
        void setCategories(List<Category> categoryList);
    }

}
