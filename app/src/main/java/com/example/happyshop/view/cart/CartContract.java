package com.example.happyshop.view.cart;

import com.example.happyshop.model.Cart;
import com.example.happyshop.view.base.BasePresenter;
import com.example.happyshop.view.base.BaseView;

import java.util.List;

/**
 * Created by ahulr on 21-08-2017.
 */

public class CartContract {

    public interface Presenter extends BasePresenter {
        void loadCartItems();

        void removeItemFromCart(Cart cart);
    }

    public interface View extends BaseView<Presenter> {
        void setCartItems(List<Cart> cartList);

        void setCartTotal(int items, double total);

        void showEmptyView();
    }

}
