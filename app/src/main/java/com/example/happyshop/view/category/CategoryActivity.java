package com.example.happyshop.view.category;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.happyshop.R;
import com.example.happyshop.adapter.ProductAdapter;
import com.example.happyshop.api.APIService;
import com.example.happyshop.app.HappyShop;
import com.example.happyshop.listener.EndlessRecyclerViewScrollListener;
import com.example.happyshop.model.Category;
import com.example.happyshop.model.Product;
import com.example.happyshop.utility.Extra;
import com.example.happyshop.utility.NetworkUtils;
import com.example.happyshop.view.base.BaseActivity;
import com.example.happyshop.view.product.ProductActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryActivity extends BaseActivity implements CategoryContract.View {

    @BindView(R.id.activity_category_toolbar)
    Toolbar activityCategoryToolbar;
    @BindView(R.id.activity_category_recycler_view)
    RecyclerView activityCategoryRecyclerView;
    @BindView(R.id.activity_category)
    RelativeLayout activityCategory;
    @BindView(R.id.activity_category_progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.activity_category_filter_textview)
    TextView activityCategoryFilterTextview;
    private String category = "Category";

    @Inject
    public APIService apiService;

    private ProductAdapter productAdapter;
    private CategoryPresenter categoryPresenter;
    private int columns = 2;
    private int page = 1;

    public static Intent getCategoryIntent(Context context, String category) {
        Intent intent = new Intent(context, CategoryActivity.class);
        intent.putExtra(Extra.CATEGORY, category);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);

        setToolbar();

        if (getIntent().hasExtra(Extra.CATEGORY)) {
            category = getIntent().getStringExtra(Extra.CATEGORY);
            getSupportActionBar().setTitle(category);
        }

        if (NetworkUtils.isConnected(this)) {
            setAdapter();
            categoryPresenter.loadCategoryProducts(category, page);
        } else {
            addNoInternetLayout(this, activityCategory);
        }
    }

    public void setToolbar() {
        setSupportActionBar(activityCategoryToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
        getSupportActionBar().setTitle(category);
    }

    @Override
    public void setView() {
        HappyShop.getInstance().getAppComponent().inject(this);
        categoryPresenter = new CategoryPresenter(apiService);
        categoryPresenter.setView(this);
    }

    private void setAdapter() {

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, columns);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);

        activityCategoryRecyclerView.setLayoutManager(gridLayoutManager);
        activityCategoryRecyclerView.setHasFixedSize(true);

        productAdapter = new ProductAdapter(this);
        activityCategoryRecyclerView.setAdapter(productAdapter);

        productAdapter.setItemClickListener(new ProductAdapter.ProductClickListener() {
            @Override
            public void onProductClicked(Product product) {
                openProduct(product);
            }
        });

        activityCategoryRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                showLoader();
                categoryPresenter.loadMoreCategoryProducts(category, page);
            }
        });

    }

    private void openProduct(Product product) {
        Intent intent = ProductActivity.getStartIntent(this, product);
        startActivity(intent);
    }

    @OnClick(R.id.activity_category_filter_textview)
    public void openFilters() {
        FilterDialog filterDialog = new FilterDialog(this);
        filterDialog.show();
        filterDialog.setFilterSelectListener(new FilterDialog.FilterSelectListener() {
            @Override
            public void onFilterSelected(Category category) {
                categoryPresenter.loadCategoryProducts(category.getTitle(), page);
            }
        });
    }

    @Override
    public void showLoading() {
        addProgressLayout(this, activityCategory);
    }

    @Override
    public void hideLoading() {
        removeProgressLayout(activityCategory);
    }

    @Override
    public void showError(String s) {
        showSnackbar(activityCategory, s);
    }

    @Override
    public void setCategoryProducts(List<Product> productList) {
        productAdapter.setProducts(productList);
    }

    @Override
    public void addCategoryProducts(List<Product> productList) {
        hideLoader();
        productAdapter.addProducts(productList);
    }

    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyView() {
        addEmptyLayout(this, activityCategory, R.drawable.empty_box, getResources().getString(R.string.empty_category));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
