package com.example.happyshop.view.base;

/**
 * Created by ahulr on 21-08-2017.
 */

public interface BaseView<T> {
    void showLoading();
    void hideLoading();
    void showError(String s);
}
