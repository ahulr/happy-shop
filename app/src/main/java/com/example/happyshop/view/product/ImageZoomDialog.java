package com.example.happyshop.view.product;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.widget.FrameLayout;

import com.example.happyshop.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;

public class ImageZoomDialog extends AppCompatDialog {

    @BindView(R.id.activity_image_zoom_image_view_touch)
    ImageViewTouch activityImageZoomImageViewTouch;
    @BindView(R.id.activity_image_zoom_cancel)
    FrameLayout activityImageZoomCancel;
    private final String imageUrl;
    private final Context context;

    public ImageZoomDialog(Context context, String imageUrl) {
        super(context, R.style.DialogTheme);
        this.context = context;
        this.imageUrl = imageUrl;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_image_zoom);
        ButterKnife.bind(this);

        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(context).load(imageUrl).into(activityImageZoomImageViewTouch);
        }

    }

    @OnClick(R.id.activity_image_zoom_cancel)
    public void close() {
        dismiss();
    }
}
