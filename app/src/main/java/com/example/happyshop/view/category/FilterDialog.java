package com.example.happyshop.view.category;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.happyshop.R;
import com.example.happyshop.adapter.FilterAdapter;
import com.example.happyshop.model.Category;
import com.example.happyshop.view.home.HomePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ahulr on 05-09-2017.
 */

public class FilterDialog extends AppCompatDialog {
    @BindView(R.id.layout_filters_close)
    ImageView layoutFiltersClose;
    @BindView(R.id.layout_filters_recycler_view)
    RecyclerView layoutFiltersRecyclerView;
    @BindView(R.id.layout_filters_apply)
    TextView layoutFiltersApply;

    private Context context;
    private FilterAdapter filterAdapter;

    public interface FilterSelectListener {
        void onFilterSelected(Category category);
    }

    private FilterSelectListener filterSelectListener;

    public FilterDialog(Context context) {
        super(context, R.style.DialogTheme);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_filters);
        ButterKnife.bind(this);
        setAdapter();
    }

    private void setAdapter() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutFiltersRecyclerView.setLayoutManager(linearLayoutManager);

        filterAdapter = new FilterAdapter(context);
        filterAdapter.setCategories(HomePresenter.getCategoryList());
        layoutFiltersRecyclerView.setAdapter(filterAdapter);

        filterAdapter.setItemClickListener(new FilterAdapter.CategoryClickListener() {
            @Override
            public void onCategoryClicked(Category category) {

                if (filterSelectListener != null) {
                    filterSelectListener.onFilterSelected(category);
                    dismiss();
                }

            }
        });

    }

    public void setFilterSelectListener(FilterSelectListener filterSelectListener) {
        this.filterSelectListener = filterSelectListener;
    }

    @OnClick(R.id.layout_filters_close)
    public void closeFilters() {
        dismiss();
    }

    @OnClick(R.id.layout_filters_apply)
    public void applyFilters() {
        dismiss();
    }

}
