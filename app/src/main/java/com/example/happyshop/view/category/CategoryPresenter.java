package com.example.happyshop.view.category;

import com.example.happyshop.api.APIService;
import com.example.happyshop.model.Category;
import com.example.happyshop.model.CategoryResponse;
import com.example.happyshop.model.Product;
import com.example.happyshop.utility.NetworkUtils;
import com.example.happyshop.view.base.BaseView;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ahulr on 21-08-2017.
 */

public class CategoryPresenter implements CategoryContract.Presenter {

    private APIService apiService;
    private CategoryContract.View view;

    public CategoryPresenter(APIService apiService) {
        this.apiService = apiService;
    }

    @Override
    public void loadCategoryProducts(String category, int page) {
        view.showLoading();
        apiService.getProducts(category, page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CategoryResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull CategoryResponse categoryResponse) {
                        view.hideLoading();
                        if (categoryResponse.getProducts().size() > 0) {
                            view.setCategoryProducts(categoryResponse.getProducts());
                        } else {
                            view.showEmptyView();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.hideLoading();
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void loadMoreCategoryProducts(String category, int page) {
        apiService.getProducts(category, page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CategoryResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull CategoryResponse categoryResponse) {
                        view.addCategoryProducts(categoryResponse.getProducts());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    public void setView(BaseView view) {
        this.view = (CategoryContract.View) view;
    }
}
