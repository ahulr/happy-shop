package com.example.happyshop;

import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.happyshop.view.cart.CartActivity;
import com.example.happyshop.view.home.HomeActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by ahulr on 24-08-2017.
 */

@RunWith(AndroidJUnit4.class)
public class OpenCartTest {

    @Rule
    public ActivityTestRule<HomeActivity> homeActivityTestRule = new ActivityTestRule<>(HomeActivity.class);

    @Before
    public void setUp(){
        Intents.init();
    }

    @Test
    public void openCart() {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.action_cart)).perform(click());
        intended(hasComponent(CartActivity.class.getName()));
    }

    @After
    public void tearDown(){
        Intents.release();
    }

}
