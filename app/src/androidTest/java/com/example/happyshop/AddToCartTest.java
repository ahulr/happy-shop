package com.example.happyshop;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.example.happyshop.model.Category;
import com.example.happyshop.utility.Constants;
import com.example.happyshop.view.cart.CartActivity;
import com.example.happyshop.view.category.CategoryActivity;
import com.example.happyshop.view.home.HomeActivity;
import com.example.happyshop.view.product.ProductActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by ahulr on 24-08-2017.
 */

@RunWith(AndroidJUnit4.class)
public class AddToCartTest {

    private static MockWebServer mockWebServer;

    @Rule
    public ActivityTestRule<HomeActivity> rule = new ActivityTestRule<>(HomeActivity.class, true, false);


    @BeforeClass
    public static void startMockServer() throws Exception {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        mockWebServer.url("/");
        Constants.ENDPOINT = mockWebServer.url("/").toString();
    }


    @AfterClass
    public static void shutdownMockServer() throws Exception {
        mockWebServer.shutdown();
    }

    @Test
    public void addToCart() throws Exception {

        String fileName = "products.json";
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(RestServiceTestHelper.getStringFromFile(getInstrumentation().getContext(), fileName)));

        Intent intent = new Intent();
        rule.launchActivity(intent);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(allOf(withId(R.id.activity_home_recycler_view), isDisplayed()))
                .perform(actionOnItemAtPosition(0, click()));

        onView(allOf(withId(R.id.activity_category_recycler_view), isDisplayed()))
                .perform(actionOnItemAtPosition(0, click()));

        onView(withId(R.id.activity_product_add_cart))
                .perform(click());

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.success_cart)))
                .check(matches(isDisplayed()));

        /*onView(allOf(withId(R.id.activity_product_add_cart), isDisplayed()))
                .perform(click());*/
    }
}
