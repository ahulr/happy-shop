package com.example.happyshop.category;


import com.example.happyshop.api.APIService;
import com.example.happyshop.model.Category;
import com.example.happyshop.model.CategoryResponse;
import com.example.happyshop.model.Product;
import com.example.happyshop.util.RxSchedulersOverrideRule;
import com.example.happyshop.view.category.CategoryContract;
import com.example.happyshop.view.category.CategoryPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.collections.ArrayUtils;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by ahulr on 23-08-2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class CategoryPresenterTest {

    @Rule
    public final RxSchedulersOverrideRule overrideSchedulersRule = new RxSchedulersOverrideRule();

    @Mock
    private CategoryContract.View view;

    @Mock
    APIService apiService;

    private CategoryPresenter categoryPresenter;
    private CategoryResponse categoryResponse = new CategoryResponse(Arrays.asList(new Product(28, "Test Product #1", "Makeup", 2800.0, "image", "Description #2", true), new Product(28, "Test Product #2", "Makeup", 1500.0, "image", "Description #1", false)));
    private String category = "Makeup";
    private int page = 1;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        //mocking home presenter
        categoryPresenter = new CategoryPresenter(apiService);
        categoryPresenter.setView(view);
    }

    @Test
    public void loadCategoryFromAPI_ShowIntoView() {

        when(apiService.getProducts(category,page)).thenReturn(Observable.just(categoryResponse));
        categoryPresenter.loadCategoryProducts(category,page);

        verify(view).showLoading();
        verify(view).setCategoryProducts(categoryResponse.getProducts());
        verify(view).hideLoading();
        verify(view, never()).showError(anyString());
    }

    @Test
    public void loadCategoryFromAPI_ShowErrorView() throws Exception {

        when(apiService.getProducts(category,page)).thenReturn(Observable.error(new RuntimeException()));
        categoryPresenter.loadCategoryProducts(category,page);

        verify(view).showLoading();
        verify(view,never()).setCategoryProducts(categoryResponse.getProducts());
        verify(view).hideLoading();
        verify(view).showError(anyString());
    }

    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.reset();
    }


}
