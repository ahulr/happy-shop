package com.example.happyshop.product;

import com.example.happyshop.api.APIService;
import com.example.happyshop.model.Product;
import com.example.happyshop.model.ProductResponse;
import com.example.happyshop.util.RxSchedulersOverrideRule;
import com.example.happyshop.view.product.ProductContract;
import com.example.happyshop.view.product.ProductPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by ahulr on 23-08-2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class ProductPresenterTest {

    private Product product = new Product(28, "Test Product", "Makeup", 2800.0, "image", "Description", false);
    private ProductResponse productResponse = new ProductResponse(product);

    @Mock
    APIService apiService;

    @Mock
    private ProductContract.View view;

    private ProductPresenter productPresenter;

    @Rule
    public final RxSchedulersOverrideRule overrideSchedulersRule = new RxSchedulersOverrideRule();


    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        //mocking product presenter
        productPresenter = new ProductPresenter(apiService);
        productPresenter.setView(view);
    }

    @Test
    public void loadProductFromAPI_ShowIntoView() {

        when(apiService.getSingleProduct(28)).thenReturn(Observable.just(productResponse));
        productPresenter.loadProduct(28);

        verify(view).showLoading();
        verify(view).setProduct(productResponse.getProduct());
        verify(view).hideLoading();
        verify(view, never()).showError(anyString());
    }

    @Test
    public void loadProductFromAPI_ShowErrorView() throws Exception {

        when(apiService.getSingleProduct(28)).thenReturn(Observable.error(new RuntimeException()));
        productPresenter.loadProduct(28);

        verify(view).showLoading();
        verify(view).showError(anyString());
        verify(view, never()).setProduct(productResponse.getProduct());
    }

    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.reset();
    }

}
