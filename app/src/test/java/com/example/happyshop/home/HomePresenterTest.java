package com.example.happyshop.home;

import com.example.happyshop.R;
import com.example.happyshop.api.APIService;
import com.example.happyshop.model.Category;
import com.example.happyshop.model.ProductResponse;
import com.example.happyshop.view.home.HomeContract;
import com.example.happyshop.view.home.HomePresenter;
import com.example.happyshop.view.product.ProductContract;
import com.example.happyshop.view.product.ProductPresenter;
import com.google.common.collect.Lists;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by ahulr on 23-08-2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class HomePresenterTest {

    @Mock
    private HomeContract.View view;

    private HomePresenter homePresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        //mocking home presenter
        homePresenter = new HomePresenter();
        homePresenter.setView(view);
    }

    @Test
    public void loadCategories_setCategories() {

        homePresenter.loadCategories();
        verify(view).setCategories(homePresenter.getCategoryList());
        verify(view, never()).showError(anyString());

    }

    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.reset();
    }

}
